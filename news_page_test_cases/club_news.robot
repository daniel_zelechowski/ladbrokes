*** Settings ***
Documentation     A test suite for opening club news.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../keywords/general_resource.robot
Suite Setup       Open Browser To Main Page
Test Teardown     Open Championship Menu
Suite Teardown    Close Browser

*** Test Cases ***
Premiership club news opens correctly
	[Template]    Open Premiership Club News Page
	1    Aberdeen
	2    Celtic
	3    Dundee
	4    Dundee United
	5    Hamilton
	6    Hearts
	7    Inverness CT
	8    Kilmarnock
	9    Motherwell
	10   Partick Thistle
	11   Ross County
	12   St Johnstone

Championship club news opens correctly
	[Template]    Open Championship Club News Page
	1    Alloa Athletic
	2    Dumbarton
	3    Falkirk
	4    Hibernian
	5    Livingston
	6    Morton
	7    Queen of the South
	8    Raith Rovers
	9    Rangers
	10   St Mirren
