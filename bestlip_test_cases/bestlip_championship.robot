*** Settings ***
Documentation     A test suite for placing bets on selected Championship Teams.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../keywords/general_resource.robot
Suite Setup       Open Browser To Main Page
Test Teardown     Remove Bet
Suite Teardown    Close Browser


*** Test Cases ***
Bet on the home team
	Select A Championship Team From The Map    Hibernian
	Place A Bet On   Home Team
	Input Stake    10
	Potential Return Should Be Calculated

Bet on a draw
	Select A Championship Team From The Map    Rangers
	Place A Bet On    Draw
	Input Stake    123
	Potential Return Should Be Calculated

Bet on the away team
	Select A Championship Team From The Map    Morton
	Place A Bet On    Away Team
	Input Stake    7000
	Potential Return Should Be Calculated