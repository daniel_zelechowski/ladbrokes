*** Settings ***
Documentation     Documentation     A test suite for placing bets on selected Premiership Teams.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../keywords/general_resource.robot
Suite Setup       Open Browser To Main Page
Test Teardown     Remove Bet
Suite Teardown    Close Browser


*** Test Cases ***
Bet on the home team
	Select A Premiership Team From The Map    Celtic
	Place A Bet On    Home Team
	Input Stake    10
	Potential Return Should Be Calculated

Bet on a draw
	Select A Premiership Team From The Map    Inverness CT
	Place A Bet On    Draw
	Input Stake    123
	Potential Return Should Be Calculated

Bet on the away team
	Select A Premiership Team From The Map    Kilmarnock
	Place A Bet On    Away Team
	Input Stake    7000
	Potential Return Should Be Calculated