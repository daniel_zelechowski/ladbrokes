*** Settings ***
Documentation     A test suite for opening a main page.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          ../keywords/general_resource.robot
Suite Teardown    Close Browser

*** Test Cases ***
Page opens correctly
	Open Browser To Main Page
	Main Page Should Be Open
	Authentication Options Should Be Present
	Main Menu Should Be Present
	Football Map Should Be Present
	Latest News Should Be Present
	Top Tweets Should Be Present