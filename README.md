## Ladbrokes main page tested with Robot Framework, Selenium2Library and debuglibrary 0.4. ##


## The most important notes since 2015-11-04 ##
* [2015-11-04, REFACTORING] The "Place A Bet On (Argument)" keyword was added in place of three separate "Place A Bet on (The Home Team/A Draw/The Away Team)" keywords.
* [2015-11-04, FEATURE] The club_news.robot file was added.
* [2015-11-07, ORGANIZATION] Code splitted into multiple files.