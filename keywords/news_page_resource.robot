*** Keywords ***
Open Premiership Club News Page
    [Arguments]    ${index}    ${club_name}
    Click Element    css=.ubermenu-submenu-id-239410 > li:nth-child(${index})
    Element Text Should Be    css=span.team-name    ${club_name}

Open Championship Club News Page
    [Arguments]    ${index}    ${club_name}
    Click Element    css=.ubermenu-submenu-id-240761 > li:nth-child(${index})
    Element Text Should Be    css=span.team-name    ${club_name}