*** Keywords ***
Main Page Should Be Open
    Title Should Be    SPFL Betting News | Ladbrokes.com | Sports News

Authentication Options Should Be Present
    Page Should Contain Element    css=.controls-wrapper

Main Menu Should Be Present
    Page Should Contain Element    css=#ubermenu-main-8553-primary

Football Map Should Be Present
    Page Should Contain Element    css=.map-img

Latest News Should Be Present
    Page Should Contain Element    css=.latest-vid-content

Top Tweets Should Be Present
    Page Should Contain Element    css=.top-tweets

Odds Availability
    Element Should Be Visible    css=.odds-description