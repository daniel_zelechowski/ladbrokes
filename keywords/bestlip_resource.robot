*** Keywords ***
Select A Premiership Team From The Map
    [Arguments]    ${team_name}
    ${selected_mapbox_number} =    Evaluate    ${MAPBOXES_PREMIERSHIP}["${team_name}"]
    ${mapbox_number_to_string} =    Convert To String    ${selected_mapbox_number}
    Set Test Variable    ${mapbox_number}    ${mapbox_number_to_string}
    Click Element    css=[data-box=${mapbox_number}]
    Element Should Be Visible    css=[data-box=${mapbox_number}]

Select A Championship Team From The Map
    Click Link    http://news.ladbrokes.com/spfl/scottish-championship
    [Arguments]    ${team_name}
    ${selected_mapbox_number} =    Evaluate    ${MAPBOXES_CHAMPIONSHIP}["${team_name}"]
    ${mapbox_number_to_string} =    Convert To String    ${selected_mapbox_number}
    Set Test Variable    ${mapbox_number}    ${mapbox_number_to_string}
    Click Element    css=[data-box=${mapbox_number}]
    Element Should Be Visible    css=[data-box=${mapbox_number}]

Place A Bet On
    [Arguments]    ${choice}
    ${respective div} =    Evaluate    ${BET CHOICES}["${choice}"]

    ${it_is_visible} =    Run keyword and ignore error    Element Should Be Visible    xpath=//div[@id='${mapbox_number}']/*/${respective div}/div[@onclick]

    Run Keyword If    '${it_is_visible[0]}' == 'PASS'
    ...    Click Element    xpath=//div[@id='${mapbox_number}']/*/${respective div}/div[@onclick]
    ...    ELSE
    ...    Run keywords    No Bets Available
    ...    AND
    ...    Fail    The Test is going to be terminated. No bets available yet. Run the test later or select a different team.

Input Stake
    [Arguments]    ${stake}
    Select Frame    css=iframe[id=betslip-frame]
    Wait Until Keyword Succeeds     10 sec    5 sec    Input Text    css=.stake_value    ${stake}

Potential Return Should Be Calculated
    ${potential_return} =    Get Text    css=.potential-return
    ${potential_return_float} =    Convert To Number    ${potential_return}
    ${total_stake} =    Get Text    css=#total_stake_val
    ${total_stake_float} =    Convert To Number    ${total_stake}
    ${odds} =    Get Text    css=.oddsNum
    ${no_slash} =    Replace String     ${odds}  /  ${Space}
    ${values} =     Split String     ${no_slash}
    ${first_element} =    Evaluate    ${values}[0]
    ${second_element} =    Evaluate    ${values}[-1]
    ${dividend} =    Convert To Number    ${first_element}
    ${divisor} =    Convert To Number    ${second_element}
    ${quotient} =    Evaluate    ${dividend} / ${divisor}
    ${expected_value} =     Evaluate    ${total_stake_float} + ${total_stake_float} * ${quotient}
    Should Be Equal As Numbers    ${expected_value}    ${potential_return_float}