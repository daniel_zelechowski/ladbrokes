*** Settings ***
Documentation     A resource file with reusable keywords and variables.

Library           Selenium2Library
Library           String
Library           Collections
Library           DebugLibrary
Library           My_Library.py

Resource          main_page_resource.robot
Resource          bestlip_resource.robot
Resource          news_page_resource.robot

*** Variables ***
${URL}      http://news.ladbrokes.com/spfl
${BROWSER}        Chrome
${DELAY}          0

${MAPBOXES_PREMIERSHIP}    {"Celtic": "mapbox-0", "Ross County": "mapbox-1", "Inverness CT": "mapbox-2", "Dundee United": "mapbox-3", "Hearts": "mapbox-4", "Hamilton": "mapbox-5", "Kilmarnock": "mapbox-6", "Aberdeen": "mapbox-7", "Dundee": "mapbox-8", "St Johnstone": "mapbox-9", "Partick Thistle": "mapbox-10", "Motherwell": "mapbox-11"}
${MAPBOXES_CHAMPIONSHIP}    {"St Mirren": "mapbox-0", "Rangers": "mapbox-1", "Raith Rovers": "mapbox-2", "Queen of the South": "mapbox-3", "Morton": "mapbox-4", "Livingston": "mapbox-5", "Hibernian": "mapbox-6", "Falkirk": "mapbox-7", "Dumbarton": "mapbox-8", "Alloa Athletic": "mapbox-9"}

${BET CHOICES}    {"Home Team": "div[1]", "Draw": "div[2]", "Away Team": "div[3]"}

*** Keywords ***
Open Browser To Main Page
    Open Browser    ${URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Element Should Be Visible    css=[id=spmCloseButton]
    Click Element    css=[id=spmCloseButton]

Remove Bet
    Click Element    css=img#remNew
    Unselect Frame

Open Championship Menu
    Click Link    http://news.ladbrokes.com/spfl/scottish-championship